# CarboSeq Issues

This project is solely aimed at being a repository of all possible issues related to the use of the modelToolBox whitin the CarboSeq project.

An issue can be a bug, a suggestion for improvement, a question or any other comment. We will do our best to adress those issues depending on their content.

All you need is to create an issue, see :  https://docs.gitlab.com/ee/user/project/issues/create_issues.html 

We encourage you to add labels to your issues. On top of general labels, such as "discussion", "suggestion", we have created labels for us to facilitate the treatment of issues:

- SOC_modelling : any issue related to SOC simulations once the data is loaded
- input_data : any issue related to loading the data in the model modelToolBox
- ontology : any issue related to the formating and the meaning of the input data
- SOC output : any issue related to exporting the results
- lab : any issue related to the environment (JupyterLab) made available for you to run the simulations

I case you hesitate between several labels, you may add more than one to a single issue!

You may want to sign you issues, because as this repository is fully open, you don't need to sign in and we will not necessarily be able to identify you to get back to you in case we have questions.

Happy issuing!
